/*********************************************************************
Example program for using the flash.c functions to read/write to 
program flash memory on the PIC18F45J10 to use as non-volatile storage
**********************************************************************/

#include <p18f45j10.h>
#include <flash.h>
#include <IO_pin_defs.h>



//==================================
// Note that dataFLASH is mapped to program memory address 0x7800
// Put your flash storage high in memory, so your program code doesn't run into it
#define STORAGE_START 0x7800         // start of flash area we intend to use for storage
#define ERASEBLOCK1   0x7800         // flash erase blocks are 1024=0x0400 bytes long
#define ERASEBLOCK2   0x7C00         // flash erase blocks are 1024=0x0400 bytes long
#define FLASHBLOCK1   0x7800         // flash write blocks are 64=0x0040 bytes long
#define FLASHBLOCK2   0x7840         // flash write blocks are 64=0x0040 bytes long
#define FLASHBLOCK3   0x7880         // flash write blocks are 64=0x0040 bytes long

#pragma romdata dataFLASH=STORAGE_START
rom unsigned char FLASHspace[1024];  // reserves space in memory map for erase block 

#pragma udata
unsigned char ram_data1[64];
unsigned char ram_data2[64];

//==================================
// Main routine 

#pragma code
void main(void)
{
  unsigned int test_int;
  unsigned char test_char1,test_char2;  

  ram_data1[0] = 0x54;
  ram_data1[1] = 0x65;
  ram_data1[2] = 0x73;
  ram_data1[3] = 0x74;
  ram_data1[4] = 0x20;
  ram_data1[5] = 0x44;
  ram_data1[6] = 0x41;
  ram_data1[7] = 0x54;
  ram_data1[8] = 0x41;
  ram_data1[9] = 0x66;

  
  erase_memory_block(ERASEBLOCK1);                     // erase 1024 byte block of flash memory 
  write_memory_block(ram_data1,FLASHBLOCK1);           // write 64 bytes from array into flash memory
        
  read_memory_block(ram_data2,FLASHBLOCK1);            // read 64 bytes from flash memory into array
    
  test_int = read_flash_program_memory_word(0x7802);   // read integer from flash memory address
  test_char1 = read_flash_program_memory_byte(0x7801); // read byte from flash memory address 
  test_char2 = ram_data2[2];
  
  // Wait forever
  while (1);
}
