/*********************************************************************
     flash.c - functions to read/write to program flash on PIC18F45J10   
     written in 8/2011 by Tom Carlson
**********************************************************************/
#include <p18f45j10.h>
#include <structures.h>
#include <flash.h>

// erase a 1024 byte block of program memory starting at address
// The minimum erase block is 1024 bytes (0x0400)
void erase_memory_block(unsigned int address) 
{
  char oldGIE;                    // remember state of interrupt enable 
  fracInt  fracTempValue;         // convert integers to/from bytes
  
  // set up TBLPTR
  fracTempValue.whole = address;        
  TBLPTRU = 0x00;
  TBLPTRH = fracTempValue.byte.high;
  TBLPTRL = 0x00 + fracTempValue.byte.low;
  
  // Erase 1024=0x0400 bytes
  EECON1bits.WREN = 1;
  EECON1bits.FREE = 1;
  oldGIE = INTCONbits.GIE;  // remember interrupts
  INTCONbits.GIE = 0;       // disable interrupts
  EECON2 = 0x55;
  EECON2 = 0x0AA;
  EECON1bits.WR = 1;
  INTCONbits.GIE = oldGIE; // return interrupts to state we found them
}

// write contents of 64 byte ram array to flash program memory starting at flash_address 
void write_memory_block(unsigned char ram_array[],unsigned int address)
{
  char oldGIE;                    // remember state of interrupt enable 
  fracInt  fracTempValue;         // convert integers to/from bytes
  int tbloffset;

  
  // set up TBLPTR
  fracTempValue.whole = address;        
  TBLPTRU = 0x00;
  TBLPTRH = fracTempValue.byte.high;
  TBLPTRL = 0x00 + fracTempValue.byte.low;
  
  // Write the 64 byte block to flash
  for (tbloffset=0; tbloffset< 64; tbloffset++){
    WRITE_TBLPOSTINC(ram_array[tbloffset]);   
  }
  
  
  EECON1bits.WREN = 1;
  oldGIE = INTCONbits.GIE;  // remember interrupts
  INTCONbits.GIE = 0;       // disable interrupts
  EECON2 = 0x55;
  EECON2 = 0x0AA;
  EECON1bits.WR = 1;
  INTCONbits.GIE = oldGIE; // return interrupts to state we found them
  EECON1bits.WREN = 0;  
}

// read contents of a 64 byte flash program memory block at address to a ram array 
void read_memory_block(unsigned char ram_array[],unsigned int address)
{
  fracInt  fracTempValue;         // convert integers to/from bytes
  int tbloffset =0; 
  
  // set up TBLPTR
  fracTempValue.whole = address;        
  TBLPTRU = 0x00;
  TBLPTRH = fracTempValue.byte.high;
  TBLPTRL = 0x40 + fracTempValue.byte.low;
  
  // Read the 64 byte block from flash
  for (tbloffset=0; tbloffset< 64; tbloffset++){
    ram_array[tbloffset] =  READ_TBLPOSTINC();
  }
}  

// pass the address of the byte you wish to read
unsigned char read_flash_program_memory_byte(unsigned int address)
{
  fracInt  fracTempValue;         // convert integers to/from bytes
  
  // set up TBLPTR
  fracTempValue.whole = address;        
  TBLPTRU = 0x00;
  TBLPTRH = fracTempValue.byte.high;
  TBLPTRL = 0x40 + fracTempValue.byte.low;

  return READ_TBLPOSTINC(); 
}

// pass the address of the word you wish to read
unsigned int read_flash_program_memory_word(unsigned int address)
{
  fracInt  fracTempValue;         // convert integers to/from bytes
  
  // set up TBLPTR
  fracTempValue.whole = address;        
  TBLPTRU = 0x00;
  TBLPTRH = fracTempValue.byte.high;
  TBLPTRL = 0x40 + fracTempValue.byte.low;
  
  // read from table
  fracTempValue.byte.low = READ_TBLPOSTINC();
  fracTempValue.byte.high = READ_TBLPOSTINC();
  return fracTempValue.whole; 
}

// Load byte into TABLAT from Table
// TBLPTR is incremented after the read
// return value of TABLAT
unsigned char READ_TBLPOSTINC(void)
{
  _asm
  TBLRDPOSTINC
  _endasm
  return TABLAT;
}

// Write byte into TABLAT 
// Load TABLAT into Table
// TBLPTR is incremented after the write
void WRITE_TBLPOSTINC(unsigned char value)
{
  TABLAT = value;
  _asm
  TBLWTPOSTINC
  _endasm 
}
