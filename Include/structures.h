/*****************************************************************
     structures.h - Useful data structures for variable manipulation      
     written in 1/98 by Tom Carlson
*******************************************************************/

// this structure is very nice for parsing integers into their low and high bytes
typedef struct {
  unsigned int   low   :8,     // low byte of int
                 high  :8;     // high byte of int
               } byte_type;   
                
typedef union  {   
  unsigned int   whole;        // this allows programmer access to entire integer
  byte_type      byte;         // or each byte of it       
               } fracInt;      // fractured Int type

