
//==================================
// Flash.c function prototypes

void erase_memory_block(unsigned int address);
void write_memory_block(unsigned char ram_array[],unsigned int address);
void read_memory_block(unsigned char ram_array[],unsigned int address);
unsigned char read_flash_program_memory_byte(unsigned int address);
unsigned int read_flash_program_memory_word(unsigned int address);
unsigned char READ_TBLPOSTINC(void);
void WRITE_TBLPOSTINC(unsigned char value);
