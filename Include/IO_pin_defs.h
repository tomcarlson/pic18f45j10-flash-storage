//  Microcontroller I/O port definitions

//Port A
#define BATT_VOLTAGE          PORTAbits.AN0 // RA0, Pin 19
#define RA1                   PORTAbits.RA1 // RA1, Pin 20
#define RA2                   PORTAbits.RA2 // RA2, pin 21
#define RA3                   PORTAbits.RA3 // RA3, pin 22
#define RESET                 PORTAbits.RA5 // RA5, Pin 24

//Port B
#define TEST1_INPUT           PORTBbits.RB0 //RB0, pin 8
#define CHARGING              PORTBbits.RB1 //RB1, pin 9
#define SPI_INT               PORTBbits.RB2 //RB2, pin 10
#define PTT3_SW               PORTBbits.RB3 //RB3, pin 11
#define RB4                   PORTBbits.RB4 //RB4, pin 14
#define CHARGE_COMPLETE       PORTBbits.RB5 //RB5, pin 15
#define RB6                   PORTBbits.RB6 //RB6, pin 16
#define RB7                   PORTBbits.RB7 //RB7, pin 17


//PORT C
#define RC0                   PORTCbits.RC0 //RC0, pin 32
#define TEST4_INPUT           PORTCbits.RC1 //RC1, pin 35
#define POWER_LED             PORTCbits.RC2 //RC2, pin 36
#define PTT_SW                PORTCbits.RC3 //RC3, pin 37
#define CHARGE_LED            PORTCbits.RC4 //RC4, pin 42
#define PAIRING_LED           PORTCbits.RC5 //RC5, pin 43
#define CVM_TX                PORTCbits.RC6 //RC6, pin 44
#define CVM_RX                PORTCbits.RC7 //RC7, pin 1

//PORT D
#define SPI_CLK               PORTDbits.RD0 //RD0, pin 38
#define SPI_DATA_OUT          PORTDbits.RD1 //RD1, pin 39
#define SPI_DATA_IN           PORTDbits.RD2 //RD2, pin 40
#define PTT2_SW               PORTDbits.RD3 //RD3, pin 41
#define POWER_SW              PORTDbits.RD4 //RD4, pin 2
#define PWR_HOLD              PORTDbits.RD5 //RD5, pin 3
#define PA_ON                 PORTDbits.RD6 //RD6, pin 4
#define SHUTDOWN              PORTDbits.RD7 //RD7, pin 5

//PORT E
#define CVM_POWER             PORTEbits.RE0 //RE0, pin 25
#define TEST2_INPUT           PORTEbits.RE1 //RE1, pin 26
#define TEST3_INPUT           PORTEbits.RE2 //RE2, pin 27

