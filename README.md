# What is here?
This is an MPLAB project directory, set up to compile using MCC18 to a [PIC18F45J10](http://search.digikey.com/scripts/DkSearch/dksus.dll?Detail&name=PIC18F45J10-I/PT-ND) microcontroller.

# What does it do?
The purpose of this is to show the usage of the flash.c functions, which are used to read and write to flash program memory.

This is useful when you need non-volatile storage of settings and sensor readings but you are using a PIC such as the [PIC18F45J10](http://search.digikey.com/scripts/DkSearch/dksus.dll?Detail&name=PIC18F45J10-I/PT-ND) which has no EEPROM.

# What do I do with it?
Probably, you will just include flash.c into your own projects and then use the functions in it as shown here.

# Other

## License

[The MIT License](http://www.opensource.org/licenses/mit-license.php)

